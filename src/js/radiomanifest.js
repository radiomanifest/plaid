import radiomanifest from "@radiomanifest/radiomanifest";

class Controller {
  constructor() {
    this.cache = {};
    this.currentRadio = null;
  }

  getFromCache(url) {
    console.log("cache", Object.keys(this.cache));
    if (this.cache[url]) {
      this.currentRadio = this.cache[url];
      return this.cache[url];
    }
  }
  async get(url) {
    if (this.cache[url]) {
      this.currentRadio = this.cache[url];
      return this.cache[url];
    }
    this.cache[url] = await radiomanifest.get(url);
    return this.cache[url];
  }
}

const ControllerSingleton = new Controller();

export default ControllerSingleton;
