import { defineStore } from "pinia";

export const useRadioStore = defineStore("radio", {
  state: () => {
    return {
      radios: {
        "https://www.ondarossa.info": {
          name: "ROR",
        },
        "https://radiospore.oziosi.org": {
          name: "Spore",
        },
      },
    };
  },
  getters: {
    isRadioReady: (state) => {
      return (url) => {
        if (state.radios[url] === undefined) {
          return false;
        }
        if (state.radios[url].lastFetch === undefined) {
          return false;
        }
        // XXX: if lastFetch is very old, return false
        return true;
      };
    },
  },
  actions: {
    startFetchingRadio(url) {
      return;
    },
    updateRadio(url, data) {
      this.radios[url] = data;
      this.radios[url].lastFetch = Date.now();
    },
  },
});
