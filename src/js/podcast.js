import getPodcastFromFeed from "podparse";

/* Avoids "mixed content" problems, hoping for the best */
function httpsIze(url) {
  if (location.protocol === "https:" && url.startsWith("http:")) {
    return url.replace("http:", "https:");
  }
  return url;
}

async function getPodcast(url) {
  let feedContent = await fetch(httpsIze(url));
  feedContent = await feedContent.text();
  return await getPodcastFromFeed(feedContent);
}

export default getPodcast;
