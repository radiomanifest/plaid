/* eslint-env node */
import HomePage from "../pages/home.vue";
import RadioPage from "../pages/Radio.vue";
import RadioLivePage from "../pages/RadioLive.vue";
import RadioShowsPage from "../pages/RadioShows.vue";
import ShowPage from "../pages/Show.vue";
import NotFoundPage from "../pages/404.vue";

var routes = [
  {
    path: process.env.NODE_ENV === "development" ? "/" : "/plaid/",
    master: true,
    component: HomePage,
    detailRoutes: [
      {
        name: "Radio",
        path: "/radio/:radioUrl",
        component: RadioPage,
      },
      {
        name: "RadioLive",
        path: "/radio/:radioUrl/live",
        component: RadioLivePage,
      },
      {
        name: "RadioShows",
        path: "/radio/:radioUrl/shows",
        component: RadioShowsPage,
      },
      {
        name: "ShowPage",
        path: "/radio/:radioUrl/shows/:showId",
        component: ShowPage,
      },
    ],
  },
  {
    path: "(.*)",
    component: NotFoundPage,
  },
];

export default routes;
